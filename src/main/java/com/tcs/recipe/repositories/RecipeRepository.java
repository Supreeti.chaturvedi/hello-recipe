package com.tcs.recipe.repositories;

import org.springframework.data.repository.CrudRepository;

import com.tcs.recipe.model.Recipe;

public interface RecipeRepository extends CrudRepository<Recipe, Long> {

}
