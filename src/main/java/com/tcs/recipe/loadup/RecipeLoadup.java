package com.tcs.recipe.loadup;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.tcs.recipe.model.Ingredient;
import com.tcs.recipe.model.Recipe;
import com.tcs.recipe.repositories.RecipeRepository;

@Component
public class RecipeLoadup implements ApplicationListener<ContextRefreshedEvent>{
	private RecipeRepository recipeRepository;
	
	public RecipeLoadup(RecipeRepository recipeRepository) {
		super();
		this.recipeRepository = recipeRepository;
	}
	
	private List<Recipe> getRecipes(){
		List<Recipe> recipes = new ArrayList<>();
		
		Recipe recipe = new Recipe();
		recipe.setDateOfCreation(Timestamp.valueOf(LocalDateTime.now()));
		recipe.setTitle("Gajar Ka Halwa");
		recipe.setDescription("Gajar Ka Halwa");
		recipe.setNoOfPeopleServing(5);
		recipe.setVeg(true);
		
		recipe.addIngredient(new Ingredient("Flour", "Flour", "2 Kg"));
		recipe.addIngredient(new Ingredient("Carrot", "Carrot", "5 Kg"));
		recipe.addIngredient(new Ingredient("Sugar", "Sugar", "500 gm"));
		recipe.addIngredient(new Ingredient("Milk", "Milk", "20 Litre"));
		recipe.addIngredient(new Ingredient("Cream", "Cream", "2 Cups"));
		recipe.addIngredient(new Ingredient("Almonds", "Almonds", "100 grams crushed"));
		
		recipes.add(recipe);
		
		recipe = new Recipe();
		recipe.setDateOfCreation(Timestamp.valueOf(LocalDateTime.now()));
		recipe.setTitle("Milkshake");
		recipe.setDescription("Banana");
		recipe.setNoOfPeopleServing(2);
		recipe.setVeg(false);

		recipe.addIngredient(new Ingredient("Banana", "Banana", "2"));
		recipe.addIngredient(new Ingredient("Milk", "Milk", "1 Glass"));
		recipe.addIngredient(new Ingredient("Sugar", "Sugar", "As per taste"));
		recipe.addIngredient(new Ingredient("Cream", "Cream", "1 Spoon"));
		recipe.addIngredient(new Ingredient("Almonds", "Almonds", "2 crushed"));
		
		recipes.add(recipe);

		return recipes;
	}

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		recipeRepository.saveAll(getRecipes());
	}
}