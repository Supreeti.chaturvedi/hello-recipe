//= require angular
//= require angular-resource
var app = angular.module('app', []);

app.controller('RecipeController', [
		'$scope',
		'RecipeService',
		function($scope, RecipeService) {
			
			var vm = this;
			vm.model = {};
			
			$scope.showUpdateSubmit = false;
			$scope.isRecipeUpdatable = false;
			$scope.showRecipes = false;
			$scope.showEditRecipe = false;
			$scope.showCreateRecipe = false;
			$scope.showSingleRecipe = false;
			
			$scope.recipe={};
			
			$scope.recipe.pageHeading;	
			$scope.searchText = "";

			$scope.recipe.id="";
			$scope.recipe.title="";
			$scope.recipe.description="";
			$scope.recipe.veg="";
			$scope.recipe.noOfPeopleServing="";
			$scope.recipe.dateOfCreation="";
			$scope.recipe.directions="";
			$scope.recipe.imagefile="";
			$scope.recipe.ingredients=[];
			
			$scope.init = function () {
				console.log('init called');
				$scope.resetAll();
				
				$scope.showRecipes = true;
				$scope.showEditRecipe = false;
				$scope.showCreateRecipe = false;
				$scope.showSingleRecipe = false;
				$scope.isRecipeUpdatable = false;
				$scope.showUpdateSubmit = false;
				
				$scope.getAllRecipes();
			};
			
			$scope.resetAll = function (){
				$scope.recipe.id="";
				$scope.recipe.title="";
				$scope.recipe.description="";
				$scope.recipe.veg="";
				$scope.recipe.noOfPeopleServing="";
				$scope.recipe.dateOfCreation="";
				$scope.recipe.directions="";
				$scope.recipe.imagefile="";
				$scope.recipe.ingredients=[];
			}
			
			$scope.addIngredient = function() {
				var emptyDataAdd = {
					desc : "",
					portion : "",
				}
				$scope.recipe.ingredients.push(emptyDataAdd);
			}
			
			$scope.recipeSearch = function() {
				console.log("title before calling " + $scope.searchText);
				if($scope.searchText!='undefined'){
					RecipeService.searchRecipeByTitle($scope.searchText).then(function success(response) {
						if(response!=null){
							console.log("found recipe")
							$scope.message = 'Recipe found!';
							
							$scope.recipe = response.data;
							$scope.recipe.id = id;
							$scope.errorMessage = '';
							
							if(id!=null){
								console.log("Showing single recipe");
								$scope.showSingleRecipeShow();
							}
						}
						else{
							console.log("not found recipe .... initiing()")
							$scope.init();
						}
					}, function error(response) {
						$scope.errorMessage = 'Error Finding Recipe!';
						$scope.message = '';
					});
				}
			}
			
			$scope.updateRecipeShow = function(id) {
				console.log("updateRecipeShow id :: " + id);
				if(!$scope.recipe || !$scope.recipe.id){
					
					RecipeService.getRecipe(id).then(
					function success(response) {
						$scope.recipe = response.data;
						$scope.recipe.id = id;
						
						console.log("$scope.recipe.id :: " + $scope.recipe.id);
						console.log("$scope.recipe :: " + $scope.recipe);
						console.log("$scope.recipe.isVeg :: " + $scope.recipe.veg);
						
						$scope.recipe.pageHeading="Update Recipe";
						$scope.isRecipeUpdatable = true;
						$scope.showUpdateSubmit = true;
						$scope.showSingleRecipe = true;
						$scope.showEditRecipe = false;
						$scope.showRecipes = false;
						$scope.showCreateRecipe = false;
						
						$scope.message = '';
						$scope.errorMessage = '';
					}, function error(response) {
						$scope.message = '';
						if (response.status === 404) {
							$scope.errorMessage = 'Recipe not found!';
						} else {
							$scope.errorMessage = "Error getting recipe!";
						}
					});
				}
				
				console.log("updateRecipeShow :: " + $scope.recipe.id);
				
				$scope.recipe.pageHeading="Update Recipe";
				$scope.isRecipeUpdatable = true;
				$scope.showUpdateSubmit = true;
				$scope.showSingleRecipe = true;
				$scope.showEditRecipe = false;
				$scope.showRecipes = false;
				$scope.showCreateRecipe = false;
			}
			
			$scope.showSingleRecipeShow = function() {
				$scope.recipe.pageHeading="Recipe Details";
				$scope.showSingleRecipe = true;
				$scope.showEditRecipe = false;
				$scope.showRecipes = false;
				$scope.showCreateRecipe = false;
				$scope.isRecipeUpdatable = false;
				$scope.showUpdateSubmit = false;
			}
			
			$scope.saveImage = function () {
				var file = $scope.myFile;
		        console.log('file is ' );
		        console.dir(file);
		        
				var id = $scope.recipe.id;
				console.log('saveImage : recipe id is ' + id);
				RecipeService.saveImage($scope.recipe).then(
						function success(response) {
							$scope.recipe = response.data;
							$scope.recipe.id = id;
							
							$scope.resetAll();
							$scope.getRecipe(id);
							
							$scope.message = '';
							$scope.errorMessage = '';
						}, function error(response) {
							$scope.message = '';
							if (response.status === 404) {
								$scope.errorMessage = 'Recipe not found!';
							} else {
								$scope.errorMessage = "Error getting recipe!";
							}
						});
			}
			
			$scope.getImage = function(data){
				var binary = '';
			    var bytes = new Uint8Array( data );
			    var len = bytes.byteLength;
			    for (var i = 0; i < len; i++) {
			        binary += String.fromCharCode( bytes[ i ] );
			    }
			    
			    var base64 = 'data:image/jpeg;base64,' + (window.btoa( binary ));
			    $scope.recipe.imagefile = base64;
			    return base64;
			}
			
			$scope.getRecipeImage = function(id) {
				RecipeService.getRecipeImage(id).then(
						function success(response) {
							console.log("got Recipe Image");
							var bytes = response.data;
						  /*var base64String = String.fromCharCode.apply(null, new Uint8Array(bytes));
						  $scope.recipe.imagefile = "data:image/jpg;base64," + base64String;*/
							$scope.getImage(bytes);
							
						    
							$scope.errorMessage = '';
				}, function error(response) {
					$scope.errorMessage = 'Error getting Recipe Image!';
					$scope.message = '';
				}).catch(function(e){
	                console.log("Error: ", e);
	                throw e;
	            });
			}

			$scope.getRecipe = function(id) {
				//var id = $scope.recipe.id;
				//console.log('recipe id is ' + $scope.recipe.id);
				console.log('recipe id is passed ' + id);
				
				RecipeService.getRecipe(id).then(
						function success(response) {
							$scope.recipe = response.data;
							$scope.recipe.id = id;
							
							console.log("$scope.recipe.id :: " + $scope.recipe.id);
							console.log("$scope.recipe :: " + $scope.recipe);
							console.log("$scope.recipe.isVeg :: " + $scope.recipe.veg);
							$scope.message = '';
							$scope.errorMessage = '';
							
							if(id!=null){
								console.log("Showing single recipe");
								
								$scope.getRecipeImage(id);
								$scope.showSingleRecipeShow();
							}
							else{
								console.log("Showing create recipe");
								$scope.createRecipeShow();
							}
							
						}, function error(response) {
							$scope.message = '';
							if (response.status === 404) {
								$scope.errorMessage = 'Recipe not found!';
							} else {
								$scope.errorMessage = "Error getting recipe!";
							}
						});
			}
			
			$scope.updateRecipe = function(id, title) {
				console.log('updateRecipe id is passed ' + id);
				
				RecipeService.updateRecipe($scope.recipe).then(
						function success(response) {
							console.log("got sucess response ");
							$scope.message = 'Recipe data updated!';
							
							
							//console.log("calling image save on :: " + $scope.recipe.imagefile);
							if($scope.recipe.imagefile){
								$scope.saveImage();
							}
							
							$scope.errorMessage = '';
							
							if(!$scope.recipe.imagefile){
								console.log("resetting all ");
								$scope.resetAll();
								$scope.getRecipe(id);
							}
							/*$scope.resetAll();
							$scope.getRecipe(id);*/
				}, function error(response) {
					$scope.errorMessage = 'Error updating Recipe!';
					$scope.message = '';
				}).catch(function(e){
	                console.log("Error: ", e);
	                throw e;
	            });
			}
			
			$scope.createRecipeShow = function() {
				console.log('createRecipeShow called');
				$scope.recipe.pageHeading="Create New Recipe";
				
				$scope.resetAll();
				
				$scope.showCreateRecipe = true;
				$scope.showEditRecipe = false;
				$scope.showRecipes = false;
				$scope.showSingleRecipe = false;
				$scope.isRecipeUpdatable = true;
				$scope.showUpdateSubmit = false;

				$scope.recipe.dateOfCreation = new Date();
			}

			$scope.temp;
			$scope.addRecipe = function(title, desc) {
				console.log('addRecipe called for :: ' + title);
				//if ($scope.recipe != null && $scope.recipe.title) {
					RecipeService.addRecipe($scope.recipe).then(
							function success(response) {
								$scope.message = 'Recipe added!';
								
								$scope.temp = $scope.recipe.imagefile; 
								$scope.recipe = response.data;
								console.log("response " + $scope.recipe.id);
								
								$scope.recipe.imagefile = $scope.temp;
								$scope.saveImage();
								
								$scope.errorMessage = '';
								//$scope.init();
								
							}, function error(response) {
								$scope.errorMessage = 'Error adding recipe!';
								$scope.message = '';
							});
				/*} else {
					$scope.errorMessage = 'Please enter a title!';
					$scope.message = '';
				}*/
			}

			$scope.deleteRecipe = function(id) {
				console.log("deleting recipe with id :: " + id);
				RecipeService.deleteRecipe(id).then(
						function success(response) {
							console.log("recipe deleted with id :: " + id)
							$scope.message = 'Recipe deleted!';
							
							$scope.init();
							
							$scope.recipe = null;
							$scope.errorMessage = '';
						}, function error(response) {
							$scope.errorMessage = 'Error deleting recipe!';
							$scope.message = '';
						})
			}

			$scope.getAllRecipes = function() {
				RecipeService.getAllRecipes().then(function success(response) {
					$scope.recipes = response.data;
					
					$scope.message = '';
					$scope.errorMessage = '';
				}, function error(response) {
					$scope.message = '';
					$scope.errorMessage = 'Error getting recipes!';
				});
			}

		} ]);

app.service('RecipeService', [ '$http', function($http) {

	this.getRecipe = function getRecipe(recipeId) {
		return $http({
			method : 'GET',
			url : '/api/recipe/id/' + recipeId
		});
	}
	
	
	function dataURItoBlob(dataURI) {
	    
	    // convert base64/URLEncoded data component to raw binary data held in a string
	    var byteString;
	    if (dataURI.split(',')[0].indexOf('base64') >= 0)
	        byteString = atob(dataURI.split(',')[1]);
	    else
	        byteString = unescape(dataURI.split(',')[1]);

	    // separate out the mime component
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

	    // write the bytes of the string to a typed array
	    var ia = new Uint8Array(byteString.length);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }

	    return new Blob([ia], {type:mimeString});

	}
	
	this.saveImage = function saveImage(recipe) {
		/*var fd = new FormData();
        fd.append('imagefile', imagefile);
		
		var uploadUrl = '/api/recipe/saveImage/' + recipeId;
		
		console.log("saveImage with post called with recipe :: " + id + " :: URLupdate :: " + uploadUrl);
		 
		return $http.post(uploadUrl, fd, {
	        withCredentials: true,
	        headers: {'Content-Type': undefined },
	        transformRequest: angular.identity
	    });*/
		
		/*return $http({
			method : 'POST',
			url : '/api/recipe/saveImage/' + recipeId,
			data : {
				imagefile : fd,
				withCredentials: true,
				headers: {
					"Content-Type": undefined
				},
				transformRequest: angular.identity
			}
		});*/
		
		var recipeId = recipe.id;
		var uploadUrl = '/api/recipe/saveImage/' + recipeId;
		
		var fd = new FormData();

		if(recipe.imagefile){
		    var imgBlob = dataURItoBlob(recipe.imagefile);
		    fd.append('imagefile', imgBlob);
	
		    /*var studentObject = self.getStudentObject();
		    fd.append('student', JSON.stringify(studentObject));*/
	
		    return $http.post(uploadUrl,fd,{
		        headers: {'Content-Type': undefined},
		        transformRequest: angular.identity,
	
		    });
		}
	}

	this.addRecipe = function addRecipe(recipe) {
		var id = recipe.id;
		
		//var newIngredientsObj = JSON.parse(JSON.stringify(recipe.ingredients))
		
		console.log("service called for recipe title :: " + recipe.title);
		var dataAdd = {
				id : recipe.id,
				title : recipe.title,
				description : recipe.description,
				isVeg : recipe.veg,
				veg : recipe.veg,
				noOfPeopleServing : recipe.noOfPeopleServing,
				dateOfCreation : recipe.dateOfCreation,
				directions : recipe.directions,
				imagefile : recipe.imagefile,
				ingredients : recipe.ingredients 
		};
		
		var dataAddjson = JSON.stringify(dataAdd);
		
		return $http({
			method : 'POST',
			url : '/api/recipe/',
			data : dataAddjson,
			
			headers: {
				"Content-Type": "application/json;charset=UTF-8",
				"Access-Control-Allow-Origin": "self",
			    "cache-control": "no-cache",
			},
			credentials: 'include',
			trasformRequest : angular.identity
		})
	}

	this.deleteRecipe = function deleteRecipe(id) {
		return $http({
			method : 'DELETE',
			url : '/api/recipe/' + id
		})
	}

	this.updateRecipe = function updateRecipe(recipe) {
		var id = recipe.id;
		
		var urlUpdate = '/api/recipe/' + id;
		
		console.log("update with put called with recipe :: " + id + " :: URLupdate :: " + urlUpdate);
		console.log("recipe.veg in update is :: " + recipe.veg);
		
		var dataj = {
				id : recipe.id,
				title : recipe.title,
				description : recipe.description,
				isVeg : recipe.veg,
				veg : recipe.veg,
				noOfPeopleServing : recipe.noOfPeopleServing,
				dateOfCreation : recipe.dateOfCreation,
				directions : recipe.directions,
				imagefile : recipe.imagefile,
				ingredients : recipe.ingredients
		};
		
		var datajson = JSON.stringify(dataj)
		//return $http.put(urlUpdate, JSON.stringify(data));
		
		return $http({
			method : 'PUT',
			url : urlUpdate,
			data : datajson, 
		
			headers: {
				"Content-Type": "application/json;charset=UTF-8",
				"Access-Control-Allow-Origin": "self",
			    "cache-control": "no-cache",
			},
			credentials: 'include',
			trasformRequest : angular.identity
		})
	}

	this.getAllRecipes = function getAllRecipes() {
		return $http({
			method : 'GET',
			url : '/api/recipe/'
		});
	}
	
	this.getRecipeImage = function getRecipeImage(recipeId) {
		return $http({
			method : 'GET',
			url : '/api/recipe/recipeimage/' + recipeId
		});
	}
	
	this.searchRecipeByTitle = function searchRecipeByTitle(recipeTitle) {
		return $http({
			method : 'GET',
			url : '/api/recipe/name/' + recipeTitle
		});
	}

} ]);

app.config(['$qProvider', function ($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);

app.directive('fileModel', ['$parse', function ($parse) { 
    return { 
        restrict: 'A', 
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel); 
            var modelSetter = model.assign;
            element.bind('change', function(){ 
                scope.$apply(function(){
                  modelSetter(scope, element[0].files[0]);
                }); 
            }); 
        } 
    }; 
}]);

app.directive("fileread", [
	  function() {
		    return {
		      scope: {
		        fileread: "="
		      },
		      link: function(scope, element, attributes) {
		        element.bind("change", function(changeEvent) {
		          var reader = new FileReader();
		          reader.onload = function(loadEvent) {
		            scope.$apply(function() {
		              scope.fileread = loadEvent.target.result;
		            });
		          }
		          reader.readAsDataURL(changeEvent.target.files[0]);
		        });
		      }
		    }
		  }
		]);